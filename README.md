# QR Scanner Demo

This is a demo UI5 app showing how to use the [custom UI5 control / library QR Scanner](https://gitlab.com/tobias.a.hofmann/qr-scanner). The app can be run using [UI5 tooling](https://sap.github.io/ui5-tooling/).

## Run app

Clone the repository and make sure you have ui5 tooling installed (npm install --global @ui5/cli).

```sh
npm install
ui5 serve
```

Access the app at http://localhost:8080

A sample QR code can be obtained from the [Wikipedia about QR codes](https://en.wikipedia.org/wiki/QR_code).

![sample](https://upload.wikimedia.org/wikipedia/commons/d/d0/QR_code_for_mobile_English_Wikipedia.svg)

## Screenshots

Note that the camera shows brackets as well as marks the identified QR code with green borders.

![demo1](./demo1.png)

![demo2](./demo2.png)

## Credits
This project has been generated with 💙 and [easy-ui5](https://github.com/SAP)
