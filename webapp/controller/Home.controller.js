/*
 * (c) Copyright Tobias Hofmann
 * https://www.itsfullofstars.de
 * Licensed under the Apache License, Version 2.0 - see LICENSE.txt.
 */
sap.ui.define([
  "de/itsfullofstars/QRScannerDemo/controller/BaseController",
  "sap/ui/model/json/JSONModel",
  "sap/ui/core/Fragment"
], function(Controller, JSONModel, Fragment) {
  "use strict";

  return Controller.extend("de.itsfullofstars.QRScannerDemo.controller.Home", {

    onInit: function () {
      this._initQrCodeModel();
    },

    /**
     * Opens a dialog to scan QR codes
     * Uses an XML fragment for the dialog.
     * @public
     */
    openQrCodeDialog: function() {
      this._stopCamera();
      if (!this._oQrCodeDialog) {
        sap.ui.core.Fragment.load({
          id: "scanQrCode",
          name: "de.itsfullofstars.QRScannerDemo.view.fragments.QRCodeDialog",
          controller: this
        }).then(function (oDialog) {
          this._oQrCodeDialog = oDialog;
          this._oQrCodeDialog.setModel(this.getView().getModel("qrcode"));
          this._oQrCodeDialog.open();
        }.bind(this));
      } else {
        this._oQrCodeDialog.open();
      }
    },

    /**
		 * Close dialog used to scan a QR code
		 * @public
		 */
		onQrCodeDialogClose: function (pressEvent) {
      // get fragment with QR scanner control and stop the camera
			var oScanner = Fragment.byId("scanQrCode", "scanner");
      oScanner.stopCamera();
      // close the dialog
			this._oQrCodeDialog.close();
			this._oQrCodeDialog.destroy(true);
			this._oQrCodeDialog = undefined;
		},
    
    /**
     * Event handler for the scanned event of the UI control QR scanner.
     * The scanned text from the QR code can be retrieved from the value parameter of the event.
     * This method will make the scanned QR code text available in the model qrcode in the property QRCode.
     * @param {*} oEvent 
     * @public
     */
    onQRCodeScanned: function(oEvent){
			var sValue = oEvent.getParameter("value");
      this.getView().getModel("qrcode").setProperty("/QRCode", sValue); 
    },

    /**
     * Resets the QR code value in the model qrcode.
     * Allows to re-scan a QR code and see the result.
     * @public
     */
    clearQrCode: function() {
      this._initQrCodeModel();
    },

    /**
     * Closes the QR code camera capture
     * Allows to see how to release the camera after a QR code is scanned and validated
     * @public
     */
    closeQrCode: function() {
      this._stopCamera();
    },

    /**
     * 
     * Internal methods
     * 
    **/

    /**
     * Stops the camera access of the QR code control.
     * @private
     */
    _stopCamera: function() {
      var oScanner = this.byId("qrscanner");
			oScanner.stopCamera();
    },

    /**
     * Initialize the QR code model
     * @private
     */
    _initQrCodeModel: function() {
      var oQrCode = new JSONModel({QRCode:"Reading QR code ..."});
			this.getView().setModel(oQrCode, "qrcode");
    }

  });
});
