/* eslint-disable no-eval */
/*!
 * Tobias Hofmann
 * Contact: https://www.itsfullofstars.de
 */
sap.ui.define([
	"./../library", 
	"sap/ui/core/Control", 
	"./QRScannerRenderer",
	"../libs/jsQR"
], function (library, Control, QRScannerRenderer, jsQRlib) {
	"use strict";

	/**
	 * Constructor for a new QRScanner control.
	 *
	 * @param {string} [sId] id for the new control, generated automatically if no id is given
	 * @param {object} [mSettings] initial settings for the new control
	 *
	 * @class
	 * Some class description goes here.
	 * @extends sap.ui.core.Control
	 *
	 * @author Tobias Hofmann
	 * @version 1.0.0
	 *
	 * @constructor
	 * @public
	 * @alias de.itsfullofstars.qrscanner.controls.QRScanner
	 * @ui5-metamodel This control/element also will be described in the UI5 (legacy) designtime metamodel
	 */
	var QRScanner = Control.extend("de.itsfullofstars.qrscanner.controls.QRScanner", {
		metadata: {
			library: "de.itsfullofstars.qrscanner",
			properties: {
				"value": {
					type: "string",
					defaultValue: null
				},
				/**
				 * Width of the preview window in pixels
				 */
				"width": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "640px"
				},
				/**
				 * Height of the preview window in pixels
				 */
				"height": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "480px"
				},
				/**
				 * Width of the video capture window in pixels
				 */
				"videoWidth": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "1280px"
				},
                /**
                 * Height of the video capture window in pixels
                 */
                "videoHeight": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "960px"
	            }				
			},
			events: {
				/**
				 * Event is fired when the user clicks on the control.
				 */
				scanned: {
                    parameters: {
                        value: {
                            type: "string"
                        }
                    }
                }
			}
		},
		
		renderer: QRScannerRenderer,
		
		init: function () {
            this._displayingVideo = false;
        },
        
        
    	/**
		 * Stops the camera.
		 * No new images will be captured. Should be called by the UI5 app when the QR code was successfully read.
		 * @public
		 */
		stopCamera: function(){
			console.log("stopCamera");
			var oVideo = this._getVideo();
			if (oVideo.srcObject){
				oVideo.srcObject.getVideoTracks().forEach( function(t){ t.stop(); });
			}
		},
        
        /**
         * Starts video capture from the camera
         * Started after the view is rendered. The user must give permission to access camera (one time).
         * Only works when the browser is supporting navigator.mediaDevices.getUserMedia
         * Only video will be capture, no audio. Video is set to environment camera
         * The camera will continuously capture through the requestAnimationFrame
         * @public
         */
        onAfterRendering: function (evt) {
        	try {
	        	var oVideo = this._getVideo();
			    if (oVideo && !this._displayingVideo) {
	                navigator.mediaDevices.getUserMedia({
	                    video: { facingMode: "environment" },
	                    audio: false
	                })
	                .then(function(stream) {
						oVideo.srcObject = stream;
						oVideo.setAttribute("playsinline", true);
						oVideo.play();
						requestAnimationFrame(this._startCapture.bind(this));
	                }.bind(this))
	                .catch(function(err) {
	                    console.log("onAfterRendering: error");
	                    console.log(err);
	                });
	            }
        	} catch(err) {
        	}
		},
		
		/**
		 * Called when UI5 is cleaning up resources.
		 * Will end the capturing of images through the camera.
		 * @public
		 */
		onExit: function() {
			console.log("QRScanner: onExit");
			this.stopCamera();	
		},
		
		/**
		 * Starts capturing images from camera
		 * This is a continuous process. The user needs to give permission on the first access for the browser
		 * to access the camera.
		 * After a QR code is found, the camera will continue to capture images. The developer of the UI5 app is 
		 * responsible to end the capturing process.
		 * @private
		 */
		_startCapture: function() {
			var oImageData = null;
			var oVideo = this._getVideo();
			
			if (oVideo) {
				if (oVideo.readyState === oVideo.HAVE_ENOUGH_DATA) {
		        	
					var width = parseInt(this.getVideoWidth(), 10);
		            var height = parseInt(this.getVideoHeight(), 10);
		            var canvas = this._getCanvas();
			    	
			    	if (width && height) {
		                canvas.drawImage(oVideo, 0, 0, width, height);
		                oImageData = canvas.getImageData(0, 0, width, height);
		                // check for QR code image. Uses jsQR library
				        var code = jsQR(oImageData.data, oImageData.width, oImageData.height, {
				          inversionAttempts: "dontInvert",
				        });
				        if (code) {
				        	// highlight the QR code and fire the scanned event
			            	this._highlightQrCode(code.location.topLeftCorner, code.location.topRightCorner, "green");
							this._highlightQrCode(code.location.topRightCorner, code.location.bottomRightCorner, "green");
							this._highlightQrCode(code.location.bottomRightCorner, code.location.bottomLeftCorner, "green");
							this._highlightQrCode(code.location.bottomLeftCorner, code.location.topLeftCorner, "green");
							this.fireScanned({
						        value: code.data
						    });
			            }
		            }
				}
				requestAnimationFrame(this._startCapture.bind(this));
			}
	    },
	    
	    /**
		 * Retrieves the canvas element from the DOM
		 * @private
		 */
    	_getVideo: function() {
    		if (this.getDomRef()) {
        		return this.getDomRef().lastElementChild;
        	} else {
        		return null;
        	}
    	},
    	
    	/**
    	 * Retrieves the canvas element from the DOM
    	 * @private
    	 */
		_getCanvas: function() {
			var canvasElement = this.getDomRef().getElementsByTagName("canvas")[0];
	    	var canvas = canvasElement.getContext('2d');
	    	return canvas;
		},
		
		/**
		 * Highlight the identified QR code in the image
		 * Draws a line around the QR code in the given color
		 * @private
		 */
		_highlightQrCode: function(begin, end, color) {
			var canvas = this._getCanvas();
			canvas.beginPath();
			canvas.moveTo(begin.x, begin.y);
			canvas.lineTo(end.x, end.y);
			canvas.lineWidth = 4;
			canvas.strokeStyle = color;
			canvas.stroke();
		}
		
    });
	return QRScanner;
}, /* bExport= */ true);