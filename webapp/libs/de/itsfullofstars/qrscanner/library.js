/*!
 * Tobias Hofmann
 * Contact: https://www.itsfullofstars.de
 */

/**
 * Initialization Code and shared classes of library de.itsfullofstars.qrscanner.
 */
sap.ui.define(["sap/ui/core/library"], // library dependency
	function () {

		"use strict";

		/**
		 * QR scanner
		 *
		 * @namespace
		 * @name de.itsfullofstars.qrscanner
		 * @author Tobias Hofmann
		 * @version 1.0.0
		 * @public
		 */

		// delegate further initialization of this library to the Core
		sap.ui.getCore().initLibrary({
			name: "de.itsfullofstars.qrscanner",
			version: "1.0.0",
			dependencies: ["sap.ui.core"],
			types: [],
			interfaces: [],
			controls: [
				"de.itsfullofstars.qrscanner.controls.QRScanner"
			],
			elements: []
		});

		/* eslint-disable */
		return de.itsfullofstars.qrscanner;
		/* eslint-enable */

	}, /* bExport= */ false);